package cn.smile.repository.core.test;

import cn.smile.repository.core.CoreRepositoryApplication;
import cn.smile.repository.core.mapper.CoreAdminMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @description:
 * @author: 龙逸
 * @createDate: 2020/08/19 21:09:06
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CoreRepositoryApplication.class)
public class CoreAdminTests {

    @Resource
    private CoreAdminMapper coreAdminMapper;

    @Test
    public void testConnection(){
        coreAdminMapper.selectCount(null);
    }
}
