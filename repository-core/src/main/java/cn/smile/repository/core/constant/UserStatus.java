package cn.smile.repository.core.constant;

/**
 * <p>
 * 应用内共享常量：用户状态
 * </p>
 *
 * @author 龙逸
 * @since 2020-08-19 20:43:21
 */
public final class UserStatus {
    /**
     * 已启用
     */
    public static final int ACTIVE = 1;

    /**
     * 已禁用
     */
    public static final int CLOSED = 0;
}
