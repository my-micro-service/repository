package cn.smile.repository.core.mapper;

import cn.smile.commons.bean.domain.core.CoreCursor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.cursor.Cursor;

/**
 * <p>
 * MyBatis流式查询Demo Mapper 接口
 * </p>
 *
 * @author 龙逸
 * @since 2020-12-31
 */
public interface CoreCursorMapper extends BaseMapper<CoreCursor> {

    /**
     * MyBatis流式查询Demo
     *
     * @param limit 分页参数
     * @return 查询结果迭代器
     */
    @Select("SELECT * FROM core_cursor LIMIT #{limit}")
    Cursor<CoreCursor> scan(@Param("limit") int limit);

}
