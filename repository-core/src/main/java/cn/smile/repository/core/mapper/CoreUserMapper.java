package cn.smile.repository.core.mapper;

import cn.smile.commons.bean.domain.core.CoreUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author 龙逸
 * @since 2020-08-20
 */
public interface CoreUserMapper extends BaseMapper<CoreUser> {

}
