package cn.smile.repository.core.mapper;

import cn.smile.commons.bean.domain.core.IndexMapping;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页映射表 Mapper 接口
 * </p>
 *
 * @author 龙逸
 * @since 2021-02-24
 */
public interface IndexMappingMapper extends BaseMapper<IndexMapping> {

}
