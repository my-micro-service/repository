package cn.smile.repository.core.mapper;

import cn.smile.commons.bean.domain.core.CoreAdmin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 管理员表 Mapper 接口
 * </p>
 *
 * @author 龙逸
 * @since 2020-08-19 20:41:32
 */
@Mapper
@Repository
public interface CoreAdminMapper extends BaseMapper<CoreAdmin> {
}
