package cn.smile.repository.core.mapper;

import cn.smile.commons.bean.domain.core.ShiroSession;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-29
 */
@SuppressWarnings({"SpellCheckingInspection"})
public interface ShiroSessionMapper extends BaseMapper<ShiroSession> {

}
