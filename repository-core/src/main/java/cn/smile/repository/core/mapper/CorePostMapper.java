package cn.smile.repository.core.mapper;

import cn.smile.commons.bean.domain.core.CorePost;
import cn.smile.commons.bean.dto.core.CorePostDTO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 文章表 Mapper 接口
 * </p>
 *
 * @author 龙逸
 * @since 2020-10-28
 */
public interface CorePostMapper extends BaseMapper<CorePost> {

    /**
     * 根据条件查询
     *
     * @param page         分页参数
     * @param queryWrapper 查询条件
     * @return 查询结果
     */
    IPage<CorePostDTO> listByQuery(IPage<CorePostDTO> page, @Param(Constants.WRAPPER) Wrapper<CorePostDTO> queryWrapper);

}
