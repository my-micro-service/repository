package cn.smile.repository.core;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * 数据处理层
 * </p>
 *
 * @author 龙逸
 * @since 2020-08-19 20:34:08
 */
@SpringBootApplication
@MapperScan(basePackages = "cn.smile.repository.core.mapper")
public class CoreRepositoryApplication {

    private static final Logger logger = LoggerFactory.getLogger(CoreRepositoryApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(CoreRepositoryApplication.class, args);
        logger.info("---------------------CoreRepositoryApplication Successful Start---------------------");
    }
}
