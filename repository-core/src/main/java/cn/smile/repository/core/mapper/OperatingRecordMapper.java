package cn.smile.repository.core.mapper;

import cn.smile.commons.bean.domain.core.OperatingRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-29
 */
public interface OperatingRecordMapper extends BaseMapper<OperatingRecord> {

}
