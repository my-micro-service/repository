package cn.smile.repository.core.mapper;

import cn.smile.commons.bean.domain.core.PrivilegeUserGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-28
 */
public interface PrivilegeUserGroupMapper extends BaseMapper<PrivilegeUserGroup> {

}
