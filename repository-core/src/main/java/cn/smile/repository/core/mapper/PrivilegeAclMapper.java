package cn.smile.repository.core.mapper;

import cn.smile.commons.bean.domain.core.PrivilegeAcl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 龙逸
 * @since 2021-06-28
 */
public interface PrivilegeAclMapper extends BaseMapper<PrivilegeAcl> {

    /**
     * 通过角色列表获取ACL列表
     *
     * @param groupIds 组列表 '12','23'
     * @return 查询结果
     */
    List<PrivilegeAcl> getAclListByGroupIds(@Param("groupIds") List<Long> groupIds);
}
